const db = require('../utils/database');

class Cat {
  constructor(id, name, age, breed) {
    this.id = id;
    this.name = name;
    this.age = age;
    this.breed = breed;
  }

  save() {
    return db.execute(
      'INSERT INTO cat (name, age, breed) VALUES (?, ?, ?)',
      [this.name, this.age, this.breed]
    );
  }

  static deleteById(id) {
      return db.execute('DELETE FROM cat where cat.id=?',[id]);
  }

  static fetchAll() {
    return db.execute('SELECT * FROM cat');
  }

  static findById(id) {
    return db.execute('SELECT * FROM cat WHERE id = ?', [id]);
  }

  static searchByFilter(age_lte,age_gte){
    const sql='select * from cat where age <= ? or age >= ?';
    return db.execute(sql,[ age_lte, age_gte ])
  }

  static findByIdAndUpdate(id,name,age,breed){
     return db.execute(`UPDATE CAT 
     SET name  = ?, 
         age   = ?, 
         breed = ?
     WHERE id = ?`, [name,age,breed,id]);
  }
};

module.exports = Cat;