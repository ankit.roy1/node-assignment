const express=require('express');
const router=express.Router();
const { 
    getAllCats,
    createCat,
    getCatById,
    searchCats,
    updateCatById,
    deleteCatById
} =require('./../controllers/catsController');

router
    .route('/')
    .get(getAllCats)
    .post(createCat)

router
    .route('/search')
    .get(searchCats)

router
    .route('/:id')
    .get(getCatById)
    .put(updateCatById)
    .delete(deleteCatById)

module.exports=router