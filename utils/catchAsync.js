const catchAsync = fn => {
    return (req,res,next)=>{
        fn(req,res,next).catch(err=>next(err))
        //the above line can also be written as
        //fn(req,res,next).catch(next)
    }
}

module.exports=catchAsync;