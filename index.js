process.on('uncaughtException',err=>{
    console.log('Uncaught Exception...')
    console.log(err)
    process.exit(1)
})

const dotenv=require('dotenv');
const db=require('./utils/database');
const express=require('express');
const catRouter=require('./routes/catsRoutes');
const AppError=require('./utils/appError')
const globalErrorHandler=require('./controllers/errorController')
dotenv.config()
const app=express()


app.use(express.urlencoded({extended:false}))
app.use('/api/v1',catRouter)

app.all('*',(req,res,next)=>{
    next(new AppError(`Can't find ${req.originalUrl} on this server`,404))
})

app.use(globalErrorHandler)


const PORT=process.env.PORT || 5000
const server=app.listen(PORT,(err)=>{
    if(err){
        console.log(`Something went wrong!!! : ${err}`)
        return;
    }
    console.log(`Server running on PORT ${PORT}`)
})

process.on('unhandledRejection',err => {
    console.log('Unhandled Rejection..')
    console.log(err.name,err.message)
    server.close(()=>{
        process.exit(1)
    })
})