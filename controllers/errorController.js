const sendErrorDev = (err,res) => {
    res.status(err.statusCode).json({
        status:err.status,
        message:err.message,
        stack:err.stack,
        error:err
    })
}

const sendErrorProd = (err,res) => {
    if(err.isOperational){
        res.status(err.statusCode).json({
            status:err.status,
            message:err.message
        })
    }
    else
    {
        //1.Log the Error
        console.error(`Error : ${err}`)
        //2.Send a generic message
        res.status(500).json({
            status:'error',
            message: 'Something went very wrong!!'
        })
    }
}

module.exports=(err,req,res,next)=>{
    err.statusCode=err.statusCode || 500
    err.status=err.status || 'error'
     
    if(process.env.NODE_ENV==='development'){
        sendErrorDev(err,res);
    }
    else if(process.env.NODE_ENV==='production'){
        sendErrorProd(err,res);
    }
}