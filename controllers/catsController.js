const db=require('../utils/database');
const Cat =require('../models/Cat');
const AppError = require('./../utils/appError');
const catchAsync=require('./../utils/catchAsync');

const createCat = catchAsync(async (req,res,next)=>{
    const {name,age,breed} = req.body

    const cat=new Cat(null,name,age,breed)
    const newRes = await cat.save()
    
    const newCat = await Cat.findById(newRes[0].insertId)

    res.status(200).json({
        status: 'success',
        results: 1,
        data: {
          data:newCat[0]
        }
    });
})

const getAllCats = catchAsync(async (req,res,next)=>{
    const cats= await Cat.fetchAll();

    res.status(200).json({
        status: 'success',
        results: cats[0].length,
        data:cats[0]
    });
})

const getCatById= catchAsync(async (req,res,next)=>{
    const id=req.params.id;

    if(isNaN(id))
        return next(new AppError(`Invalid id : ${id}`))
    
    const result = await Cat.findById(id)

    if(result[0].length===0){
        return next(new AppError(`Cat with the id :${id} does not exist`,404))
    }

    res.status(200).json({
        status: 'success',
        results: 1,
        data:result[0]
    });
})

const searchCats = catchAsync(async (req,res,next)=>{
    const { age_lte, age_gte } = req.query
    const result = await Cat.searchByFilter(age_lte, age_gte)
    
    res.status(200).json({
        status: 'success',
        results: result[0].length,
        data:result[0]
    });
})

const deleteCatById = catchAsync(async (req,res,next)=>{
    const id=req.params.id
    const result=await Cat.deleteById(id);

    if(result[0].affectedRows===0){
        return next(new AppError(`Cat with id : ${id} does not exist`,404))
    }
    
    res.status(204).json({
        status:"success",
        data:null
    })
})


const updateCatById = catchAsync(async (req,res,next)=>{
        const {id} = req.params
    
        let cat = await Cat.findById(id)

        if(cat[0].length===0){
            return next(new AppError(`Cat with id : ${id} does not exist`,404))
        }

        let { name,age,breed } = req.body
        name  = name  || cat[0][0].name
        age   = age   || cat[0][0].age
        breed = breed || cat[0][0].breed

        const updatedCat= await Cat.findByIdAndUpdate(id,name,age,breed)
        
        let result = await Cat.findById(id)

        res.status(200).json({
            status: 'success',
            results: 1,
            data:result[0]
        });
})

module.exports = {
    createCat,
    getAllCats,
    getCatById,
    searchCats,
    deleteCatById,
    updateCatById
}